<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function() use($router) {
    $router->get('questions', ['uses' => 'QuestionController@getAll']);
    $router->get('question/{id}/answers', ['uses' => 'QuestionController@getAnswers']);
    $router->post('question/create', ['uses' => 'QuestionController@create']);
    $router->put('question/{id}/update', ['uses' => 'QuestionController@update']);
    $router->post('answer/create', ['uses' => 'AnswerController@create']);
    $router->put('answer/{id}/update', ['uses' => 'AnswerController@update']);
    $router->post('answer/{id}/vote', ['uses' => 'AnswerController@vote']);

});
