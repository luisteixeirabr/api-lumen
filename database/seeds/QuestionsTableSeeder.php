<?php

use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            'title' => 'Qual seleção será a campeã da Copa da Russia?',
        ]);

        DB::table('questions')->insert([
            'title' => 'Qual candidado você votaria para próximo Presidente do Brasil?',
        ]);
    }
}
