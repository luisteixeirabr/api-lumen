<?php

use Illuminate\Database\Seeder;

class AnswersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('answers')->insert([
            'title' => 'Brasil',
            'question_id' => 1,
            'votes' => 0,
            'status' => true
        ]);

        DB::table('answers')->insert([
            'title' => 'Alemanhã',
            'question_id' => 1,
            'votes' => 0,
            'status' => true
        ]);

        DB::table('answers')->insert([
            'title' => 'Bolsonaro',
            'question_id' => 2,
            'votes' => 0,
            'status' => true
        ]);

        DB::table('answers')->insert([
            'title' => 'Alckmin',
            'question_id' => 2,
            'votes' => 0,
            'status' => true
        ]);
    }
}
