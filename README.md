# api Lumen

API Rest para criar Questionarios e Respostas

Para instalar:

- clonar o projeto: git clone https://luisteixeirabr@bitbucket.org/luisteixeirabr/api-lum.git
- entrar na pasta do projeto: cd api-lumen
- composer install

Se disponível o servidor embutido executar: php -S localhost:8000 -t public
Ou apontar seu servidor Apache para a pasta public

Rotas:
http://localhost:8000/api/

GET: /questions
GET: /question/{id}/answers
POST: /question/create
POST: /answer/create
PUT: /answer/{id}/update
POST: /answer/{id}/vote
