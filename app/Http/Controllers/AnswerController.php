<?php

namespace App\Http\Controllers;

use App\Answer;
use Illuminate\Http\Request;

class AnswerController extends Controller
{
    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'question_id' => 'required'
        ]);

        $answer = Answer::create($request->all());

        return response()->json($answer, 201);
    }

    public function update($id, Request $request)
    {
        $answer = Answer::findOrFail($id);
        $answer->update($request->all());

        return response()->json($answer, 200);
    }

    public function vote($id)
    {
        $answer = Answer::findOrFail($id);
        $answer->update(['votes', $answer->votes++]);

        return response()->json($answer, 200);
    }

    public function delete($id)
    {
        Answer::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }
}
