<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function getAll()
    {
        return response()->json(Question::with('answers')->get());
    }

    public function getAnswers($id)
    {
        $question = Question::find($id);

        return $question->answers()->get();
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $question = Question::create($request->all());

        return response()->json($question, 201);
    }

    public function update($id, Request $request)
    {
        $question = Question::findOrFail($id);
        $question->update($request->all());

        return response()->json($question, 200);
    }

    public function delete($id)
    {
        Question::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    public function editStatus($id, Request $request)
    {
        $this->validate($request, [
            'status' => 'required',
        ]);

        $question = Question::findOrFail($id);

        if ($question) {
            $question->update($request);
        }

        return response()->json($question, 200);
    }
}
